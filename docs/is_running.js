#!/usr/bin/env osascript -l JavaScript

// see https://stackoverflow.com/questions/59310170/how-to-check-if-process-is-already-running-by-name-in-jxa
// 

ObjC.import('stdlib')

function run(argv) {
  sys = Application('com.apple.SystemEvents');
  sys.processes.name();

  var name = argv.join(' ');
  if (sys.processes[name].exists()) {
    console.log(name + " found");
  } else {
    console.log(name + " NOT found");
  }
}
