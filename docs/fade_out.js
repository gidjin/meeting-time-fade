#!/usr/bin/env osascript -l JavaScript

ObjC.import("stdlib");

function run(argv) {
  var app = Application.currentApplication();
  app.includeStandardAdditions = true;

  // Basic api calls
  //app.setVolume(1) // 0 = muted, 7 = full volume
  //app.setVolume(null, {outputVolume: 100}); // 0 = muted, 7 = full volume
  //app.setVolume(null, { outputMuted: true });
  //app.setVolume(null, { outputMuted: false });

  // response from app.getVolumeSettings();
  // {"outputVolume":50,"inputVolume":100,"alertVolume":100,"outputMuted":false}

  // Grab current volume so we can go back
  var prevVolume = app.getVolumeSettings();

  var avoidFullMute = argv.find((o) => o === "--avoid-full-mute" || o === "-a");
  var textIdx = argv.findIndex((o) => o === "--text" || o === "-t");
  var text =
    textIdx >= 0 && textIdx < argv.length ? argv[textIdx + 1] : "Time is Up";

  // Fade down to zero over some amount of time...
  //app.setVolume(null, { outputMuted: true });
  var prevVolume = prevVolume.outputVolume || 50;
  var newVolume = prevVolume.outputVolume || 50;
  console.log("Time to fade out...");
  for (let i = prevVolume; i >= 0; i--) {
    app.setVolume(null, { outputVolume: newVolume });
    delay(0.2);
    newVolume--;
    // if one of the specified apps is running then only go down to 1/2 volume
    if (avoidFullMute && newVolume <= prevVolume / 2) {
      break;
    }
  }

  // wait a couple more moments and reset volume
  console.log("wait for it");
  delay(15);
  console.log("And we're back");
  app.setVolume(null, { outputVolume: prevVolume });
  if (avoidFullMute) {
    app.doShellScript("afplay -v 2 /System/Library/Sounds/Ping.aiff");
    app.doShellScript("afplay -v 2 /System/Library/Sounds/Submarine.aiff");
    app.doShellScript("afplay -v 2 /System/Library/Sounds/Ping.aiff");
    app.doShellScript("afplay -v 2 /System/Library/Sounds/Submarine.aiff");
  } else {
    app.say(text);
  }
}
