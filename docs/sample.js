#!/usr/bin/env osascript -l JavaScript

ObjC.import("stdlib");
ObjC.import("EventKit");

function run(argv) {
  console.log(JSON.stringify(argv));

  var app = Application.currentApplication();
  app.includeStandardAdditions = true;
  // app.say("Go to your meeting")
  //$.MPMediaPlayback.alloc.pause();

  store = $.EKEventStore.alloc.initWithAccessToEntityTypes($.EKEntityMaskEvent);
  eventCalendars = store.calendarsForEntityType($.EKEntityTypeEvent);

  var output = "";

  for (var i = 0; i < eventCalendars.count; i++) {
    var calendar = eventCalendars.objectAtIndex(i);
    output +=
      ObjC.unwrap(calendar.title) +
      "(" +
      ObjC.unwrap(calendar.type) +
      "): " +
      ObjC.unwrap(calendar.calendarIdentifier);
    output += "\n";
  }

  console.log(output);
  /*
  var hoursInFuture = 10;

  	store = $.EKEventStore.alloc.initWithAccessToEntityTypes($.EKEntityMaskEvent)
	predicateStart = $.NSDate.alloc.initWithTimeIntervalSinceNow(1)
	predicateEnd = $.NSDate.alloc.initWithTimeIntervalSinceNow(60*60*hoursInFuture)
	predicate = store.predicateForEventsWithStartDateEndDateCalendars(predicateStart, predicateEnd, [])

	events = store.eventsMatchingPredicate(predicate)

	output = ""

	for (var i = 0; i < events.count; i++) {
		var event = events.objectAtIndex(i)
		output += ObjC.unwrap(event.title) + ", " + ObjC.unwrap(event.startDate) + ", " + ObjC.unwrap(event.eventIdentifier) + " <" + ObjC.unwrap(event.calendar.title) + ">" + "\n"
	}

	console.log(output)

*/

  /*
  console.log(app.currentDate())
  var Calendar = Application("Calendar")
  if (Calendar !== undefined) {
    console.log("failed to get calendar")
    return
  }
  var workCals = Calendar.calendars.whose({name: "john@truss.works"})
  if (workCals === undefined) {
    console.log("failed to get workCals")
    return
  }
  console.log(JSON.stringify(workCals))

 var startOfDay = new Date();
    startOfDay.setHours(0);
    startOfDay.setMinutes(0);
    startOfDay.setSeconds(0);
    startOfDay.setMilliseconds(0);
    var endOfDay = new Date();
    endOfDay.setHours(23);
    endOfDay.setMinutes(59);
    endOfDay.setSeconds(59);
    endOfDay.setMilliseconds(999);
console.log(startOfDay)
console.log(endOfDay)
    var events = Application('Calendar').calendars.events.whose({
        _and: [
            { startDate: { _greaterThan: startOfDay }},
            { startDate: { _lessThan: endOfDay }}
        ]
    });
  console.log(JSON.stringify(events))
  */
  /*
    var convertedEvents = events();
    for (var cal of convertedEvents) {
        for (var ev of cal) { 
            console.log(ev.summary());
        }
    }
  */

  // $> osascript -e "set volume output volume [0-100]"
  // $> osascript -e "set volume output muted true"
  // var finder = Application('Finder');
  //
  //app.setVolume(1) // 0 = muted, 7 = full volume
  //app.setVolume(null, {outputVolume: 100}); // 0 = muted, 7 = full volume
  //var currentVolume = app.getVolumeSettings();
  // {"outputVolume":50,"inputVolume":100,"alertVolume":100,"outputMuted":false}
  //console.log(currentVolume.outputVolume); // 0 = muted, 7 = full volume
  //app.setVolume(null, { outputMuted: true });
  //delay(0.2);
  //app.setVolume(null, { outputMuted: false });

  // status = $.system(argv.join(" "))
  // $.exit(status >> 8)
}
