#!/usr/bin/env osascript -l JavaScript

// see https://stackoverflow.com/questions/59310170/how-to-check-if-process-is-already-running-by-name-in-jxa
// 

ObjC.import('stdlib')

function run(argv) {
  var app = Application.currentApplication()
  app.includeStandardAdditions = true

  const command = `echo test`

  console.log(app.doShellScript(command))
}
