#!/usr/bin/env osascript -l JavaScript

ObjC.import('stdlib')

// GENERIC FUNCTIONS ------------------------------------------------------

// doesFileExist :: String -> Bool
function doesFileExist(strPath) {
  var error = $();
  return $.NSFileManager.defaultManager
    .attributesOfItemAtPathError($(strPath)
      .stringByStandardizingPath, error), error.code === undefined;
};
// lines :: String -> [String]
function lines(s) {
  return s.split(/[\r\n]/);
};

// readFile :: FilePath -> maybe String
function readFile(strPath) {
  var error = $(),
    str = ObjC.unwrap(
      $.NSString.stringWithContentsOfFileEncodingError($(strPath)
        .stringByStandardizingPath, $.NSUTF8StringEncoding, error)
    ),
    blnValid = typeof error.code !== 'string';
  return {
    nothing: !blnValid,
    just: blnValid ? str : undefined,
    error: blnValid ? '' : error.code
  };
};

// show :: a -> String
function show(x) {
  return JSON.stringify(x, null, 2);
};

// TEST -------------------------------------------------------------------
var strPath = '~/tree.txt';

// return doesFileExist(strPath) ? function () {
//   var dctMaybe = readFile(strPath);
//   return dctMaybe.nothing ? dctMaybe.error : show(lines(dctMaybe.just));
// }() : 'File not found:\n\t' + strPath;
var fileFound = doesFileExist(strPath);

console.log(fileFound ? 'found' : 'File not found:\n\t' + strPath);
var dctMaybe = readFile(strPath);
var stuff = dctMaybe.nothing ? dctMaybe.error : show(lines(dctMaybe.just));
console.log(stuff);
console.log(JSON.stringify(dctMaybe.just));
console.log(JSON.stringify(JSON.parse(dctMaybe.just)));
