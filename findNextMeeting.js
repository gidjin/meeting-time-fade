#!/usr/bin/env osascript -l JavaScript

ObjC.import("stdlib");
ObjC.import("EventKit");

function run(argv) {
  // option hoursInFuture
  var hoursInFutureIdx = argv.findIndex((o) => o === "--hours-in-future");
  var hoursInFuture =
    hoursInFutureIdx >= 0 && hoursInFutureIdx < argv.length
      ? argv[hoursInFutureIdx + 1]
      : 8;
  // required option calendar name
  var calendarIdx = argv.findIndex((o) => o === "--calendar");
  var calendarToWatch = null;
  if (calendarIdx >= 0 && calendarIdx < argv.length) {
    calendarToWatch = argv[calendarIdx + 1];
    // console.log(`calendar: ${calendarToWatch}`);
  } else {
    console.error("--calendar <name> is required flag");
  }

  var app = Application.currentApplication();
  app.includeStandardAdditions = true;

  store = $.EKEventStore.alloc.initWithAccessToEntityTypes($.EKEntityMaskEvent);
  predicateStart = $.NSDate.alloc.initWithTimeIntervalSinceNow(60);
  predicateEnd = $.NSDate.alloc.initWithTimeIntervalSinceNow(
    60 * 60 * hoursInFuture
  );
  predicate = store.predicateForEventsWithStartDateEndDateCalendars(
    predicateStart,
    predicateEnd,
    []
  );

  events = store.eventsMatchingPredicate(predicate);

  for (var i = 0; i < events.count; i++) {
    var event = events.objectAtIndex(i);
    var calendar = ObjC.unwrap(event.calendar.title);

    // is the evnet on our calendar
    if (calendar == calendarToWatch) {
      var eventTitle = ObjC.unwrap(event.title).replace(/'/g, "");
      var start = new Date(ObjC.unwrap(event.startDate));
      var now = new Date();
      var accepted = ObjC.unwrap(event.status) === $.EKEventStatusConfirmed;
      var eventDetails = {
        title: eventTitle,
        accepted,
        start,
        lastChecked: now,
        id: ObjC.unwrap(event.eventIdentifier),
        calendar: ObjC.unwrap(event.calendar.title),
      }

      var data = JSON.stringify(eventDetails)
      //console.log(data)

      // have we accepted it and does it start in the future
      if (accepted &&
        start.getTime() > now.getTime()
      ) {
        // console.log(data)
        var command = `echo '${data}' | jq '$config[0] + { nextMeeting: . }' --slurpfile config ~/.config/calendar_alert.json | sponge ~/.config/calendar_alert.json`
        app.doShellScript(command)
        break;
      }
    }
  }
}
