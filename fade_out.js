#!/usr/bin/env osascript -l JavaScript

ObjC.import("stdlib");
ObjC.import("EventKit");

/**
 * Function that looks for the configured apps to avoid Fullly muting
 * Returns true if one is runing.
 */
function isFullMuteAppRunning({ app, config }) {
  // check if one of the specified apps is open
  sys = Application("com.apple.SystemEvents");
  sys.processes.name();
  return config.avoidFullMuteApps.some((e) => {
    if (sys.processes[e].exists()) {
      return true;
    } else {
      return false;
    }
  });
}

/**
 * Function that takes the Application.currentApplication and two flags
 * one turns off fading
 */
function fadeOut({ app, config, noFade, avoidFullMute }) {
  if (noFade) {
    return;
  }

  // Grab current volume so we can go back
  var prevVolume = app.getVolumeSettings();

  // Fade down to zero over some amount of time...
  //app.setVolume(null, { outputMuted: true });
  // outputVolume can be null if connected to an output like HDMI that the mac cannot control.
  if (prevVolume.outputVolume !== null) {
    var newVolume = prevVolume.outputVolume || 50;
    for (let i = prevVolume.outputVolume; i >= 0; i--) {
      app.setVolume(null, { outputVolume: newVolume });
      delay(0.2);
      newVolume--;
      // if one of the specified apps is running then only go down to 1/2 volume
      if (avoidFullMute && newVolume <= prevVolume.outputVolume / 2) {
        break;
      }
    }
  } else {
    // no controllable audio, for example if HDMI output is used
    delay(20);
  }

  // wait a couple more moments and reset volume
  delay(15);
  if (prevVolume.outputVolume !== null) {
    app.setVolume(null, prevVolume);
  }
}

function doesFileExist(strPath) {
  var error = $();
  return (
    $.NSFileManager.defaultManager.attributesOfItemAtPathError(
      $(strPath).stringByStandardizingPath,
      error
    ),
    error.code === undefined
  );
}

function readFile(strPath) {
  var error = $(),
    str = ObjC.unwrap(
      $.NSString.stringWithContentsOfFileEncodingError(
        $(strPath).stringByStandardizingPath,
        $.NSUTF8StringEncoding,
        error
      )
    ),
    blnValid = typeof error.code !== "string";
  return {
    nothing: !blnValid,
    just: blnValid ? str : undefined,
    error: blnValid ? "" : error.code,
  };
}

function loadConfig(configFile) {
  // check for file
  var fileFound = doesFileExist(configFile);
  if (!fileFound) {
    // TODO: write default config if not present
    throw "Config file not found: " + configFile;
  }

  var configRaw = readFile(configFile);

  if (configRaw.nothing) {
    throw "Config is empty: " + configFile + "\n\t" + configFile.error;
  }

  try {
    return JSON.parse(configRaw.just);
  } catch (e) {
    throw "Failed to parse config file: " + e;
  }
}

/**
 * Main run function. Runs the watcher to check for events and fade out as needed
 */
function run(argv) {
  var app = Application.currentApplication();
  app.includeStandardAdditions = true;

  var configFile = "~/.config/calendar_alert.json";
  // Load the config each time to allow checking of version
  var config = loadConfig(configFile);

  var noFade = argv.find((o) => o === "--no-fade");
  var textIdx = argv.findIndex((o) => o === "--text" || o === "-t");
  var text =
    textIdx >= 0 && textIdx < argv.length ? argv[textIdx + 1] : "Time is Up";

  const avoidFullMute = isFullMuteAppRunning({ app, config });

  // meeting is about to start fade out the audio
  fadeOut({ app, config, avoidFullMute, noFade });

  // Audibly announce the meeting
  if (avoidFullMute) {
    // just play simple sounds in case user is talking
    app.doShellScript("afplay -v 2 /System/Library/Sounds/Ping.aiff");
    app.doShellScript("afplay -v 2 /System/Library/Sounds/Submarine.aiff");
    app.doShellScript("afplay -v 2 /System/Library/Sounds/Ping.aiff");
    app.doShellScript("afplay -v 2 /System/Library/Sounds/Submarine.aiff");
  } else {
    // read whole event title
    app.say(text);
  }
}
