#!/usr/bin/env zsh

CONFIG_FILE=~/.config/calendar_alert.json
BASEDIR=$(dirname $0)

# time for work but no meeting yet
BEFORE_ALL_SCRIPT=$(jq --raw-output .beforeAllScript $CONFIG_FILE)
eval $BEFORE_ALL_SCRIPT

while harvest current > /dev/null; do
  CALENDAR=$(jq --raw-output .calendar $CONFIG_FILE)
  HOURSFUTURE=$(jq --raw-output .hoursInFuture $CONFIG_FILE)

  # update the next meeting
  $BASEDIR/findNextMeeting.js --calendar $CALENDAR --hours-in-future $HOURSFUTURE

  # this returned the hour and minute for next meeting but doesn't work if event is on next day
  #TIME=$(date --date "`jq --raw-output .nextMeeting.start $CONFIG_FILE` -2 minute" "+%H:%M")
  # termdown handles ISO 1806 date formats so we use date to do the math and keep the format.
  TIME=$(date --date "`jq --raw-output .nextMeeting.start $CONFIG_FILE` -2 minute" "+%FT%T%z")
  TITLE=$(jq --raw-output .nextMeeting.title $CONFIG_FILE)
  #termdown --no-seconds --alt-format --blink --quit-after 120 --critical 120 -t title $TIME
  termdown --no-seconds --alt-format --blink --quit-after 60 --critical 120 -t $TITLE $TIME

  # could be that we stopped the timer
  # if so skip fade out and any post script
  if harvest current > /dev/null ; then
    echo "Fade out for $TITLE notification..."
    $BASEDIR/fade_out.js --text "Go To $TITLE"

    POST_MEETING_FADE_SCRIPT=$(jq --raw-output .postMeetingFadeScript $CONFIG_FILE)
    eval $POST_MEETING_FADE_SCRIPT
  fi

  # take a moment to see if the user has stopped and is ready to go to the meeting
  # we ask for their input, if not then we will time out and re alert them
  # see https://unix.stackexchange.com/questions/565718/zsh-timeout-for-vared-builtin
  default=yes
  if
    # TODO: this shouldn't happen if there  is no timer

    # basically ask for input and set a time out in TMOUT
    command=$(
      saved_settings=$(stty -g)
      trap 'stty $saved_settings; exit 1' ALRM
      v=
      TMOUT=10 vared -p "Are you going? [$default]: " v &&
        printf %s $v
    )
  then
    command=${command:-$default}
    printf 'Got: "%s"\n' $command
    # user responded so snooze long enough for the meeting to start so we don't fade
    # out twice in a row
    sleep 30
  else
    # No response from the user after the TMOUT so we'll just let the loop happen 
    # and alert them again
    print timeout
  fi
done

# harvest timer stopped
AFTER_ALL_SCRIPT=$(jq --raw-output .afterAllScript $CONFIG_FILE)
eval $AFTER_ALL_SCRIPT
