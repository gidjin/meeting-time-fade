# Meeting Time Fade

Script that uses JavaScript for Automation (JXA) to check the calendar for events and fade out the audio right before it. Currently it will fade out the system volume as that seemed easiest to control.

## Usage

Current version is pretty basic and requires the cli and has a short configuration file that is required.

### 1. Setup Calendar.app

The script requires access to your calendar. To do this simply open the `Calendar.app` on your mac and set it up to sync any / all the calendars you want to have this script look at.

Make note of the calendars name if you have more than one and only want one to be looked at.

### 2. Setup a configuration file.

Save the following to `~/.config/calendar_alert.json`, be sure to modify it with the calendar you want to filter to. Note currently the script only supports filtering one calendar.

```json
{
  "calendar": "nameofcalendar@example.com",
  "hoursInFuture": 18,
  "postMeetingFadeScript": "",
  "avoidFullMuteApps": ["zoom.us", "Microsoft Teams"]
}
```

Leave the `calendar` option blank if you want the script to look at all calendars.

```json
{
  "calendar": "",
  "hoursInFuture": 18,
  "postMeetingFadeScript": "",
  "avoidFullMuteApps": ["zoom.us", "Microsoft Teams"]
}
```

The `hoursInFuture` option dictates how far ahead to look for events. Recommendation is at least 8 hours.

### 3. Run the script

In a terminal run `./calendar_alert.js` to look at all calendars for next event. The script will run indefinitely looking for the next meeting and fading out once it is ~1 minute before. It will re-look at the calendar each loop so new meetings will be picked up quickly. Note setting the sync time in `Calendar.app` settings is important otherwise this app may not see new meetings in time. Recommendation is to set it to 15 minutes.

### Debugging

If you want to see more logs run `./calendar_alert.js --debug`. `-d` is a valid short form as well. This will verbosely print messages to see what the script is doing for development and debugging purposes.

### Do not fade out the music

If you pass the `--no-fade` flag as part of the list of arguments `./calendar_alert.js --debug --no-fade` then the script will work as normal but it will not fade out the audio. Useful for testing the script while developing it.

### Show the config file

If you just want the script to show the configuration file it read you can call `./calendar_alert.js --show-config` and it will print out the result.

```sh
❯ ./calendar_alert.js --show-config
{"calendar":"nameofcalendar@example.com","hoursInFuture":18}
```

### Don't fade all the way

If you have overlapping meetings or some other app that you don't want to go all the way to mute while it is open then set `avoidFullMuteApps` in the config. If an app with the name of one of the items in this list is found to be running then the volume will only fade to half the current setting instead of all the way. Use case for this is to avoid having the script mute audio while you are in a meeting, say for back to back or overlapping meetings. If you don't know the name you can test it with `./docs/is_running.js Name of app` and it will tell you if it is found or not.

### Run arbitray command after fade out

The script will call an abitrary cli command for you if you add it to your config in `postMeetingFadeScript` like below. I use this to change the light outside my office that indicates if I'm in a meeting or not.

```json
{
  "calendar": "nameofcalendar@example.com",
  "hoursInFuture": 18,
  "postMeetingFadeScript": "curl http://192.168.1.2/example",
  "avoidFullMuteApps": ["zoom.us", "Microsoft Teams"]
}
```

NOTE if this command fails the script will exit with the error message. You can ignore it with standard shell magic as below

```json
{
  "calendar": "nameofcalendar@example.com",
  "hoursInFuture": 18,
  "postMeetingFadeScript": "curl http://192.168.1.2/example || true",
  "avoidFullMuteApps": ["zoom.us", "Microsoft Teams"]
}
```

### Bonus

You can easily fade out afer a set time with `sleep 300 ; docs/fade_out.js` which will fade out after 5 minutes.

## Design

The script will loop looking at all events in the configured number of future hours to find one that is happening within 65 seconds. If one is not found it will sleep for 5 seconds and check again. If one is found it will gradually fade out the system volume, remain muted for 15 seconds, and then restore the volume to the level it was when the fade started. After the restore it will use the Apple text to voice to audibly tell you to go to your meeting. The script then will pause for 65 seconds to avoid picking up the same meeting again.

## Docs

The docs folder contains some scripts with variety of experiments to see what I could do since JXA docs are not as clear as I'd hoped. Below are some links that were helpful too

## Resources

- https://github.com/JXA-Cookbook/JXA-Cookbook/wiki/Using-JavaScript-for-Automation#creating-a-mac-os-x-service
- https://developer.apple.com/documentation/eventkit/ekevent?language=objc
- https://github.com/tylergaw/js-osx-app-examples
- https://stackoverflow.com/questions/42296120/read-a-text-file-into-an-array-with-os-x-javascript
- https://github.com/FSecureLABS/CalendarPersist/blob/main/CalendarPersist.js
- https://stackoverflow.com/questions/59310170/how-to-check-if-process-is-already-running-by-name-in-jxa
