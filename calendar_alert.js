#!/usr/bin/env osascript -l JavaScript

ObjC.import("stdlib");
ObjC.import("EventKit");

/**
 * Version
 * script checks this against config version and exits if there is a miss match.
 * This is to facilitate restarting the script when it is run by launchd
 */
const version = "0.1";

/**
 * Function that looks for the configured apps to avoid Fullly muting
 * Returns true if one is runing.
 */
function isFullMuteAppRunning({ app, config, debug }) {
  // check if one of the specified apps is open
  sys = Application("com.apple.SystemEvents");
  sys.processes.name();
  return config.avoidFullMuteApps.some((e) => {
    debugLog(debug, "looking for " + e);
    if (sys.processes[e].exists()) {
      debugLog(debug, e + " found");
      return true;
    } else {
      debugLog(debug, e + " NOT found");
      return false;
    }
  });
}

/**
 * Function that takes the Application.currentApplication and two flags
 * one turns off fading, and one turns on debug logging.
 */
function fadeOut({ app, config, noFade, avoidFullMute, debug }) {
  if (noFade) {
    debugLog(noFade, "Would have faded out");
    return;
  }

  // Grab current volume so we can go back
  var prevVolume = app.getVolumeSettings();

  // Fade down to zero over some amount of time...
  //app.setVolume(null, { outputMuted: true });
  // outputVolume can be null if connected to an output like HDMI that the mac cannot control.
  if (prevVolume.outputVolume !== null) {
    var newVolume = prevVolume.outputVolume || 50;
    debugLog(debug, "Time to fade out...");
    for (let i = prevVolume.outputVolume; i >= 0; i--) {
      app.setVolume(null, { outputVolume: newVolume });
      delay(0.2);
      newVolume--;
      // if one of the specified apps is running then only go down to 1/2 volume
      if (avoidFullMute && newVolume <= prevVolume.outputVolume / 2) {
        break;
      }
    }
  } else {
    // no controllable audio, for example if HDMI output is used
    debugLog(debug, "Cannot fade out, just delay and warn...");
    delay(20);
  }

  // wait a couple more moments and reset volume
  debugLog(debug, "wait for a short moment to disrupt focus");
  delay(15);
  if (prevVolume.outputVolume !== null) {
    debugLog(debug, "Restore volume");
    app.setVolume(null, prevVolume);
  }
}

/**
 * Debug logging helper function. Takes a boolean and a message. If boolean
 * is true message is logged.
 */
function debugLog(debug, message) {
  if (debug) {
    console.log(message);
  }
}

/**
 * Following two functions based on https://stackoverflow.com/questions/42296120/read-a-text-file-into-an-array-with-os-x-javascript
 */
function doesFileExist(strPath) {
  var error = $();
  return (
    $.NSFileManager.defaultManager.attributesOfItemAtPathError(
      $(strPath).stringByStandardizingPath,
      error
    ),
    error.code === undefined
  );
}

function readFile(strPath) {
  var error = $(),
    str = ObjC.unwrap(
      $.NSString.stringWithContentsOfFileEncodingError(
        $(strPath).stringByStandardizingPath,
        $.NSUTF8StringEncoding,
        error
      )
    ),
    blnValid = typeof error.code !== "string";
  return {
    nothing: !blnValid,
    just: blnValid ? str : undefined,
    error: blnValid ? "" : error.code,
  };
}

function loadConfig(configFile) {
  // check for file
  var fileFound = doesFileExist(configFile);
  if (!fileFound) {
    // TODO: write default config if not present
    throw "Config file not found: " + configFile;
  }

  var configRaw = readFile(configFile);

  if (configRaw.nothing) {
    throw "Config is empty: " + configFile + "\n\t" + configFile.error;
  }

  try {
    return JSON.parse(configRaw.just);
  } catch (e) {
    throw "Failed to parse config file: " + e;
  }
}

/**
 * Main run function. Runs the watcher to check for events and fade out as needed
 */
function run(argv) {
  var debug = argv.find((o) => o === "--debug" || o === "-d");
  var noFade = argv.find((o) => o === "--no-fade");
  var showConfig = argv.find((o) => o === "--show-config");
  var configFile = "~/.config/calendar_alert.json";
  var expectedVersionIdx = argv.findIndex((o) => o === "--expected-version");
  var expectedVersion =
    expectedVersionIdx >= 0 && expectedVersionIdx < argv.length
      ? argv[expectedVersionIdx + 1]
      : version;

  debugLog(debug, JSON.stringify(argv));

  var app = Application.currentApplication();
  app.includeStandardAdditions = true;

  debugLog(debug, "Version = " + version);

  while (true) {
    // Load the config each time to allow checking of version
    var config = loadConfig(configFile);

    if (showConfig) {
      debugLog(showConfig, JSON.stringify(config));
      return;
    }

    // version check
    if (expectedVersion && version !== expectedVersion) {
      throw (
        "Version miss match: Script (" +
        version +
        ") !== Expected Version (" +
        expectedVersion +
        ")"
      );
    }
    debugLog(
      debug,
      "Version match: Script (" +
        version +
        ") === Expected Version (" +
        expectedVersion +
        ")"
    );

    store = $.EKEventStore.alloc.initWithAccessToEntityTypes(
      $.EKEntityMaskEvent
    );
    predicateStart = $.NSDate.alloc.initWithTimeIntervalSinceNow(60);
    predicateEnd = $.NSDate.alloc.initWithTimeIntervalSinceNow(
      60 * 60 * config.hoursInFuture
    );
    predicate = store.predicateForEventsWithStartDateEndDateCalendars(
      predicateStart,
      predicateEnd,
      []
    );

    events = store.eventsMatchingPredicate(predicate);

    var foundNextEvent = false;

    for (var i = 0; i < events.count; i++) {
      var event = events.objectAtIndex(i);
      var calendar = ObjC.unwrap(event.calendar.title);
      var start = new Date(ObjC.unwrap(event.startDate));
      var now = new Date();
      var accepted = ObjC.unwrap(event.status) === $.EKEventStatusConfirmed;
      // event.hasAttendees returns true if attendees are on the event. Useful for filtering out meetings just blocking time off
      if (
        (!config.calendar || calendar == config.calendar) &&
        accepted &&
        start.getTime() > now.getTime()
      ) {
        foundNextEvent = true;
        var left = start.getTime() - now.getTime();
        left = Math.round(left / 1000);

        var eventTitle = ObjC.unwrap(event.title);
        var hours = ("0" + start.getHours()).slice(-2);
        var minutes = ("0" + start.getMinutes()).slice(-2);
        var eventDetails = {
          title: eventTitle,
          accepted,
          start,
          timeLeft: left,
          id: ObjC.unwrap(event.eventIdentifier),
          calendar: ObjC.unwrap(event.calendar.title),
          clock: `${hours}:${minutes}`
        }
        var data = JSON.stringify(eventDetails, null, "  ")
        debugLog(debug, data);

        if (left < 65) {
          const avoidFullMute = isFullMuteAppRunning({ app, config, debug });

          // meeting is about to start fade out the audio
          fadeOut({ app, config, avoidFullMute, noFade, debug });

          // Audibly announce the meeting
          debugLog(debug, "Announce it's time to go");
          if (avoidFullMute) {
            // just play simple sounds in case user is talking
            app.doShellScript('afplay -v 2 /System/Library/Sounds/Ping.aiff');
            app.doShellScript('afplay -v 2 /System/Library/Sounds/Submarine.aiff');
            app.doShellScript('afplay -v 2 /System/Library/Sounds/Ping.aiff');
            app.doShellScript('afplay -v 2 /System/Library/Sounds/Submarine.aiff');
          } else {
            // read whole event title
            app.say("Go to " + eventTitle);
          }

          // PostMeetingFadeScript callback
          if (config.postMeetingFadeScript) {
            debugLog(debug, "Execute `" + config.postMeetingFadeScript + "`");
            var result = app.doShellScript(config.postMeetingFadeScript);
            debugLog(debug, "Result: " + result);
          }
          // Delay looking for the next meeting since fade out ends before the one we just found has actually started.
          debugLog(
            debug,
            "Wait for meeting to actually start before looking for the next one"
          );
          delay(60);
        }

        // General delay before looking if we are at a meeting
        delay(5);

        break;
      }
    }

    // TODO: how to dectect that there is nothing on the calendar for a while and what action should be taken?
    // debugLog(debug, "Finished looking at events in next " + hoursInFuture + " hours. Try again")
    // Sleep for 5 minutes if nothing within hoursInFuture
    if (!foundNextEvent) {
      debugLog(
        debug,
        "Sleep for 5 minutes after checking everything in next " +
          config.hoursInFuture +
          " hours"
      );
      delay(300);
    }
  }
}
